<?php
/**
 * @file        admin.php
 * @description
 *
 * PHP Version  5.3.13
 *
 * @package 
 * @category
 * @plugin URI
 * @copyright   2014, Vadim Pshentsov. All Rights Reserved.
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @author      Vadim Pshentsov <pshentsoff@gmail.com> 
 * @link        http://pshentsoff.ru Author's homepage
 * @link        http://blog.pshentsoff.ru Author's blog
 *
 * @created     09.05.14
 */

if(is_admin()) {
    /**
     * Админское меню, структура
     */
    function postevents_admin_menu() {
        add_menu_page(__('Postevents', POSTEVENTS_TEXT_DOMAIN), 'Postevents', 'manage_options', 'manage-postevents', 'postevents_main_page', POSTEVENTS_MENU_ICON, POSTEVENTS_MENU_POSITION);
    }
    add_action('admin_menu', 'postevents_admin_menu');

}

/**
 * Главная страница, распределительная
 */
function postevents_main_page() {

//    $action = (isset($_POST['submit_action'])&&isset($_POST['action'])) ? $_POST['action'] : false;
    $action = isset($_POST['action']) ? $_POST['action'] : false;

    if($action) {
        switch($action) {
            case 'events_list':
            case 'refresh':
                postevents_events_page();
                break;
            case 'refresh_posts':
            case 'select_post':
                postevents_select_post_page();
                break;
            case 'add_event':
            case 'edit_event':
                postevents_edit_event_page();
                break;
            case 'save_event':
                postevents_save_event();
                postevents_post_events_page();
                break;
            case 'clear_cron':
                wp_clear_scheduled_hook(POSTEVENTS_SCHEDULE_TASK);
                postevents_events_page();
                break;
            case 'delete':

                $deleted = postevents_delete_event();

                //@todo сообщение
                if($deleted) {

                } else {

                }
                postevents_events_page();

                break;
        }
    } else {
        switch('action') {
            case 'refresh_posts':
                postevents_select_post_page();
                break;
            default:
                postevents_events_page();
        }
    }
}

/**
 * Таблица событий
 */
function postevents_events_page() {

    global $wpdb;

    function _postevents_edit_events_page_actions() { ?>
        <div class="tablenav top">
            <div class="alignleft actions">
                <select name="action" style="min-width:250px;">
                    <option value="refresh"><?php _e('Refresh', POSTEVENTS_TEXT_DOMAIN); ?></option>
                    <option value="select_post"><?php _e('Add new event', POSTEVENTS_TEXT_DOMAIN); ?></option>
                    <option value="edit_event"><?php _e('Edit event', POSTEVENTS_TEXT_DOMAIN); ?></option>
                    <option value="delete"><?php _e('Delete event(s)', POSTEVENTS_TEXT_DOMAIN); ?></option>
                    <option value="clear_cron"><?php _e('Clear cron tasks', POSTEVENTS_TEXT_DOMAIN); ?></option>
                </select>
                <input type="submit" name="submit_action" value="<?php _e('Go', POSTEVENTS_TEXT_DOMAIN); ?>" class="button-secondary action" />
            </div>
        </div>
    <?php
    }

    $args = array(
        'per_page'  => (isset($_POST['per_page']) ? $_POST['per_page'] : 5),
        'offset'    => (isset($_POST['offset']) ? $_POST['offset'] : 0),
        'count'     => 0,
        'action'    => 'refresh_events',
    );

    if($wpdb->query(sprintf('SELECT COUNT(*) AS count FROM `%s` LIMIT 1', POSTEVENTS_TABLE_EVENTS))) {
        $args['count'] = array_shift($wpdb->last_result)->count;
    }

    $sql = sprintf('SELECT * FROM `%s` ORDER BY `event_date` ASC LIMIT %d, %d', POSTEVENTS_TABLE_EVENTS, $args['offset'], $args['per_page']);

    if($wpdb->query($sql)) {
        $postevents = $wpdb->last_result;
    }

    //debug - для проверки крона
//    $cron = get_option('cron');
//    $cron_prep = array();
//    foreach ($cron as $key => $task) {
//
//        if(is_array($task)) {
//
//            $schedule_time = date(POSTEVENTS_DATE_FORMAT, (int)$key);
//            $cron_prep[$schedule_time] = $task;
//
//            foreach ($task as $task_name => $schedule_data) {
//                $next = date(POSTEVENTS_DATE_FORMAT, wp_next_scheduled($task_name));
//                $cron_prep[$schedule_time]['next'] = $next;
//            }
//        }
//
//    }
//
//    print_server_time();
//    print_pre('$cron', $cron_prep);
    ?>
    <div class="wrap">
        <h2>
            <?php _e('Postevents', POSTEVENTS_TEXT_DOMAIN); ?>
            <?php
            if ( ! empty( $_REQUEST['s'] ) )
                printf( ' <span class="subtitle">' . __('Search results for &#8220;%s&#8221;') . '</span>', get_search_query() );
            ?>
        </h2>

        <form method="post" action="<?php echo $_SERVER['REQUEST_URI'];?>">

            <div class="tablenav top">
                <div class="alignleft actions">
                    <?php
                    postevents_pagination($args);
                    ?>
                </div>
            </div>

            <table class="widefat">
                <thead valign="middle" align="center">
                <th>&nbsp;</th>
                <th><?php _e('Event title', POSTEVENTS_TEXT_DOMAIN); ?></th>
                <th><?php _e('Event date', POSTEVENTS_TEXT_DOMAIN); ?></th>
                <th><?php _e('Post title', POSTEVENTS_TEXT_DOMAIN); ?></th>
                </thead>
                <tbody>
                <?php
                if(!empty($postevents)) {
                    foreach ($postevents as $event) {
                        $post = (isset($event->post_id)&&$event->post_id) ? get_post($event->post_id) : null;
                        ?>
                        <tr class="">
                            <td><input type="checkbox" name="selected_events[]" value="<?php echo $event->ID; ?>"/></td>
                            <td><?php echo $event->event_title; ?></td>
                            <td><?php echo $event->event_date; ?></td>
                            <td><?php echo (isset($post) ? $post->post_title : _e('Not set', POSTEVENTS_TEXT_DOMAIN)); ?></td>
                        </tr>
                        <?php
                    }

                } else {
                    echo '<tr><td colspan="4"><i>';
                    _e('No any events are found', POSTEVENTS_TEXT_DOMAIN);
                    echo '</i></td></tr>';
                }
                ?>
                </tbody>
            </table>

            <?php _postevents_edit_events_page_actions(); ?>

        </form>

    </div>
<?php
}

/**
 * Выбор записи для нового события
 */
function postevents_select_post_page() {

    global $wpdb;

    $args = array(
        'posts_per_page'   => (isset($_POST['per_page']) ? $_POST['per_page'] : 5),
        'offset'           => (isset($_POST['offset']) ? $_POST['offset'] : 0),
        'category'         => '',
        'orderby'          => 'post_date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        'meta_key'         => '',
        'meta_value'       => '',
        'post_type'        => 'post',
        'post_mime_type'   => '',
        'post_parent'      => '',
        'post_status'      => 'publish',
        'suppress_filters' => true );

    $posts = get_posts($args);

   ?>
    <div class="wrap">
        <h2><?php _e('Select post', POSTEVENTS_TEXT_DOMAIN); ?></h2>

        <form method="post" action="<?php echo $_SERVER['REQUEST_URI'];?>">
            <div class="tablenav top">
                <div class="alignleft actions">
                    <?php
                    postevents_pagination(array('action' => 'refresh_posts', 'per_page' => $args['posts_per_page'], 'offset' => $args['offset'], 'count' => wp_count_posts()->publish));
                    ?>
                </div>
            </div>
            <table class="widefat">
                <thead>
                <th>&nbsp;</th>
                <th><?php _e('Post title', POSTEVENTS_TEXT_DOMAIN); ?></th>
                <th><?php _e('Post author', POSTEVENTS_TEXT_DOMAIN); ?></th>
                <th><?php _e('Post date', POSTEVENTS_TEXT_DOMAIN); ?></th>
                </thead>
                <tbody>
                <?php
                if(!empty($posts)) {
                    foreach ($posts as $post) {
                        $author = get_userdata($post->post_author);
                        ?>
                        <tr>
                            <td><input type="radio" name="selected_post" value="<?php echo $post->ID; ?>"/></td>
                            <td><?php echo $post->post_title; ?></td>
                            <td><?php echo $author->data->user_nicename; ?></td>
                            <td><?php echo $post->post_date; ?></td>
                        </tr>
                        <?php
                    }

                } else {
                    echo '<tr><td colspan="4"><i>';
                    _e('No any posts are found', POSTEVENTS_TEXT_DOMAIN);
                    echo '</i></td></tr>';
                }
                ?>
                </tbody>
            </table>
            <div class="tablenav top">
                <div class="alignleft actions">
                    <input name="action" type="hidden" value="add_event"/>
                    <input type="submit" name="submit_action" value="<?php _e('Select', POSTEVENTS_TEXT_DOMAIN); ?>" class="button-secondary action" />
                </div>
            </div>
        </form>
    </div>
<?php
}

/**
 * Удвление события
 */
function postevents_delete_event($event_id = 0) {

    $deleted_count = 0;

    if(!$event_id) {
        if(isset($_POST['selected_events'])&&is_array($_POST['selected_events'])) {
            foreach ($_POST['selected_events'] as $selected_id) {
                if(_do_postevents_delete_event($selected_id)) {
                    $deleted_count++;
                }
            }
        }
    } else {
         $deleted_count = _do_postevents_delete_event($event_id);
    }

    return $deleted_count;
}

function _do_postevents_delete_event($event_id) {

    global $wpdb;

    if($event_id > 0) {
        return $wpdb->query("DELETE FROM `".POSTEVENTS_TABLE_EVENTS."` WHERE `ID`={$event_id}");
    }

    return false;

}

/**
 * Редактирование события
 */
function postevents_edit_event_page() {
    global $wpdb;

    $event_id = (isset($_POST['selected_events'])&&!empty($_POST['selected_events'])) ? $_POST['selected_events'][0] : false;

    if($event_id) {
        if($wpdb->query("SELECT * FROM `".POSTEVENTS_TABLE_EVENTS."` WHERE `ID`=$event_id")) {
            $event_data = array_shift($wpdb->last_result);
        } else {
            //@todo Error - not found with such event_id
            postevents_events_page();
            return;
        }
        $post_id = $event_data->post_id;
        $page_title = __('Edit event', POSTEVENTS_TEXT_DOMAIN);
    } else {
        $event_data = (object)array(
            'ID' => 0,
            'event_title' => '',
            'event_date' => date(POSTEVENTS_DATE_FORMAT),
        );
        $post_id = isset($_POST['selected_post']) ? $_POST['selected_post'] : 0;
        $page_title = __('Create event', POSTEVENTS_TEXT_DOMAIN);
    }

    if($post_id) {
        $post = get_post($post_id);
        $event_data->parent_post_title = $post->post_title;
        $event_data->post_id = $post_id;
    } else {
        $event_data->parent_post_title = __('Post is not set', POSTEVENTS_TEXT_DOMAIN);
    }

    $event_data->post_thumbnail = false;
    if($event_data->post_thumbnail_id) {
        $thumbnail_post = get_post($event_data->post_thumbnail_id);
        if(isset($thumbnail_post->guid)) {
        $event_data->post_thumbnail = $thumbnail_post->guid;
        }
    }

    $event_data->post_image = false;
    if($event_data->post_image_id) {
        $image_post = get_post($event_data->post_image_id);
        if(isset($image_post->guid)) {
        $event_data->post_image = $image_post->guid;
        }
    }

    ?>
    <div class="wrap">
        <h2><?php echo $page_title; ?></h2>

        <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
            <div class="floatleft padding10">
                <label for="event_title" class="width250 align-right padding5">
                    <?php _e('Event title', POSTEVENTS_TEXT_DOMAIN); ?>
                </label>
                    <input type="text" id="event_title" required="required" name="event_title" value="<?php echo $event_data->event_title; ?>" class="width250 padding5"/>
            </div>
            <div class="floatleft padding10">
                <?php _e('Parent post', POSTEVENTS_TEXT_DOMAIN); ?> : <?php echo $event_data->parent_post_title; ?>
            </div>
            <div class="clear"></div>
            <div class="padding10 curtime">
            	<span id="timestamp">
	                <input type="text" required="required" class="postevents-datetime" name="event_date" value="<?php echo mysql2date("Y-m-d", $event_data->event_date, false); ?>" />
                    <input type="text" required="required" size="2" name="event_date_hours" value="<?php echo mysql2date("H", $event_data->event_date, false); ?>" />&nbsp;:&nbsp;
                    <input type="text" size="2" name="event_date_minutes" value="<?php echo mysql2date("i", $event_data->event_date, false); ?>" />&nbsp;:&nbsp;
                    <input type="text" size="2" name="event_date_seconds" value="<?php echo mysql2date("s", $event_data->event_date, false); ?>" />
                </span>
                <br/>
                <span class="description">
                    <?php print_server_time(); ?>
                </span>
            </div>
            <div class="clear"></div>
            <div class="floatleft padding10">
                <p>
                    <label for="post_title" class="width250 padding5 align-right"><?php _e('Post title', POSTEVENTS_TEXT_DOMAIN); ?></label>
                    <input type="text" id="post_title" name="post_title" value="<?php echo $event_data->post_title; ?>" class="width250 padding5"/><br/>
                    <label for="post_url" class="width250 padding5 align-right"><?php _e('Forum URL', POSTEVENTS_TEXT_DOMAIN); ?></label>
                    <input type="text" id="post_url" name="post_url" value="<?php echo $event_data->post_url; ?>" class="width250 padding5"/><br/>
                    <input type="checkbox" id="post_featured" name="post_featured" <?php echo ($event_data->post_featured ? 'checked=checked' : ''); ?>/>
                    <label for="post_featured" class="width250 padding5 align-right"><?php _e('Featured', POSTEVENTS_TEXT_DOMAIN); ?></label>
                </p>
                <p>
                    <label for="post_image" class="width250 padding5 align-right"><?php _e('Add image', POSTEVENTS_TEXT_DOMAIN); ?></label>
                    <br/>
                    <img class="attachment-post-thumbnail wp-post-image" id="post-image-image" src="<?php echo ($event_data->post_image ? $event_data->post_image : ''); ?>" />
                    <br/>
                    <input type="hidden" name="post_image_id" value="<?php echo $event_data->post_image_id; ?>"/>
                    <a id="post-image" class="image-file-upload button insert-media add_media" title="<?php _e('Select', POSTEVENTS_TEXT_DOMAIN); ?>" data-editor="post_text" href="#">
                        <span class="wp-media-buttons-icon"></span>
                        <?php _e('Select', POSTEVENTS_TEXT_DOMAIN); ?>
                    </a>
                </p>
            </div>
            <div class="floatleft">
                <p>
                    <label for="post_text" class="width250 padding5 align-right"><?php _e('Text', POSTEVENTS_TEXT_DOMAIN); ?></label><br/>
                    <?php wp_editor($event_data->post_text, 'post_text'); ?>
                </p>
                <p>
                    <label for="post_thumbnail" class="width250 padding5 align-right"><?php _e('Thumbnail', POSTEVENTS_TEXT_DOMAIN); ?></label>
                    <br/>
                    <img class="attachment-post-thumbnail wp-post-image" id="post-thumbnail-image" src="<?php echo ($event_data->post_thumbnail ? $event_data->post_thumbnail : ''); ?>" />
                    <br/>
                    <input type="hidden" name="post_thumbnail_id" value="<?php echo $event_data->post_thumbnail_id; ?>"/>
                    <a id="post-thumbnail" class="image-file-upload button insert-media add_media" title="<?php _e('Select', POSTEVENTS_TEXT_DOMAIN); ?>" data-editor="post_text" href="#">
                        <span class="wp-media-buttons-icon"></span>
                        <?php _e('Select', POSTEVENTS_TEXT_DOMAIN); ?>
                    </a>
                </p>
            </div>
            <div class="clear"></div>
            <div class="floatleft">
                <input type="hidden" name="event_id" value="<?php echo $event_data->ID; ?>"/>
                <input type="hidden" name="post_id" value="<?php echo $event_data->post_id; ?>"/>
                <input type="hidden" name="action" value="save_event"/>
                <input type="submit" name="submit_action" value="<?php echo ($event_data->ID ? __('Save', POSTEVENTS_TEXT_DOMAIN) : __('Add', POSTEVENTS_TEXT_DOMAIN)); ?>" class="button-secondary action" />
            </div>
        </form>
    </div>
<?php
}

/**
 * Сохраняем собыьтие
 *
 */
function postevents_save_event() {

    global $wpdb;

    $event_data = (object)array(
        'event_title' => $_POST['event_title'],
//        'event_date' => date(POSTEVENTS_DATE_FORMAT, strtotime($_POST['event_date'])),
        'event_date' => $_POST['event_date'].' '.(int)substr($_POST['event_date_hours'], 0, 2).':'.(int)substr($_POST['event_date_minutes'], 0, 2).':'.(int)substr($_POST['event_date_seconds'], 0, 2),
        'post_id' => $_POST['post_id'],
        'post_title' => $_POST['post_title'],
//        'post_text' => sanitize_text_field($_POST['post_text']),
        'post_text' => $_POST['post_text'],
        'post_url' => sanitize_url($_POST['post_url']),
        'post_featured' => (isset($_POST['post_featured']) ? 1 : 0),
        'ID' => (isset($_POST['event_id']) ? $_POST['event_id'] : false),
        'post_thumbnail_id' => $_POST['post_thumbnail_id'],
        'post_image_id' => $_POST['post_image_id'],
        'event_done' => 0,
        'event_done_at' => '0000-00-00 00:00:00',
    );

    if($event_data->ID) {
        $res = $wpdb->query(sprintf("UPDATE `%s` SET
            `event_title`='%s', `event_date`='%s',
            `post_id`='%d', `post_title`='%s', `post_text`='%s', `post_url`='%s', `post_featured`=%d,
            `post_thumbnail_id`=%d, `post_image_id`=%d,
            `event_done` = 0, `event_done_at` = '0000-00-00 00:00:00'
            WHERE `ID`=%d",
            POSTEVENTS_TABLE_EVENTS,
            $event_data->event_title, $event_data->event_date,
            $event_data->post_id, $event_data->post_title, $event_data->post_text, $event_data->post_url, $event_data->post_featured,
            $event_data->post_thumbnail_id, $event_data->post_image_id,
            $event_data->ID
        ));
        if($res) {
            $res = $event_data->ID;
        }
    } else {
        $res = $wpdb->query(sprintf("INSERT `%s` SET
            `event_title`='%s', `event_date`='%s',
            `post_id`='%d', `post_title`='%s', `post_text`='%s', `post_url`='%s', `post_featured`=%d,
            `post_thumbnail_id`=%d, `post_image_id`=%d
            ",
            POSTEVENTS_TABLE_EVENTS,
            $event_data->event_title, $event_data->event_date,
            $event_data->post_id, $event_data->post_title, $event_data->post_text, $event_data->post_url, $event_data->post_featured,
            $event_data->post_thumbnail_id, $event_data->post_image_id
        ));
    }

    return $res;
}

/**
 * События для выбранного поста
 */
function postevents_post_events_page() {

    global $wpdb;

    $post_id = isset($_POST['post_id']) ? $_POST['post_id'] : false;

    if($post_id) {
        if($wpdb->query(sprintf(
            "SELECT * FROM `%s` WHERE `post_id`=%d",
            POSTEVENTS_TABLE_EVENTS, $post_id
        ))) {
            $postevents = $wpdb->last_result;
        } else {
            //@todo Not found with such post_id
            postevents_events_page();
            return;
        }
        $post = get_post($post_id);
    } else {
        //@todo не указан post_id
        postevents_events_page();
        return;
    }

    ?>
    <div class="wrap">
        <h2><?php _e('List of post events', POSTEVENTS_TEXT_DOMAIN); ?></h2>
        <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
            <?php _e('Parent post', POSTEVENTS_TEXT_DOMAIN); ?> : <?php echo $post->post_title; ?>
            <table class="widefat">
                <thead>
                <th><?php _e('Event title', POSTEVENTS_TEXT_DOMAIN); ?></th>
                <th><?php _e('Post title', POSTEVENTS_TEXT_DOMAIN); ?></th>
                <th><?php _e('Event date', POSTEVENTS_TEXT_DOMAIN); ?></th>
                </thead>
                <tbody>
                <?php

                if(!empty($postevents)){
                    foreach ($postevents as $event) {
                        ?>
                        <tr>
                            <td><?php echo $event->event_title; ?></td>
                            <td><?php echo $event->post_title; ?></td>
                            <td><?php echo $event->event_date; ?></td>
                        </tr>
                    <?php
                    }
                } else {
                    echo '<tr><td colspan="4"><i>';
                    _e('No any posts are found', POSTEVENTS_TEXT_DOMAIN);
                    echo '</i></td></tr>';
                }
                ?>
                </tbody>
            </table>
            <input type="hidden" name="selected_post" value="<?php echo $post_id; ?>"/>
            <input name="action" type="hidden" value="add_event"/>
            <input type="submit" name="events_page" value="<?php _e('To events list', POSTEVENTS_TEXT_DOMAIN); ?>" class="button-secondary action" onclick="document.getElementsByName('action')[0].value = 'events_list'; return true;"/>
            <input type="submit" name="submit_action" value="<?php _e('Add more', POSTEVENTS_TEXT_DOMAIN); ?>" class="button-secondary action" />
        </form>
    </div>
<?php
}