/**
 * Javascript source file
 * @file        postevents.js
 * @description
 *
 * @package     postevents
 * @category
 * @copyright   2014, Vadim Pshentsov. All Rights Reserved.
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @author      Vadim Pshentsov <pshentsoff@gmail.com>
 * @link        http://pshentsoff.ru Author's homepage
 * @link        http://blog.pshentsoff.ru Author's blog
 *
 * @created     12.05.14
 */

jQuery(document).ready(function() {
    jQuery('.postevents-datetime').datepicker({
        dateFormat: "yy-mm-dd"
    });

    /* Глобальные переменные для запоминания элементов ввода от вызова thickbox'a до обработки результатов */
    var el_img = "";
    var el_id = "";
    var wp_send2editor = window.send_to_editor;
    /* Обрабатываем результаты */
    var postevents_send2editor = function(html) {

        /* Берем URL изображения */
        img_url = jQuery("img", html).attr("src");

        /* Определяем id */
        img_classes = jQuery("img", html).attr("class").split(" ");
        for(i=0; i < img_classes.length; i++) {
            class_parts = img_classes[i].split("-");
            if(class_parts[0] == "wp" && class_parts[1] == "image") {
                img_post_id = class_parts[2];
            }
        }

        /* и передаем нашим запомненным элементам */
        el_img.attr("src", img_url);
        el_id.val(img_post_id);

        tb_remove();
        window.send_to_editor = wp_send2editor;
    }

    /* Обработка события .click() кнопки */
    jQuery(".image-file-upload").click(function() {

        /* наш элемент ввода - сосед типа 'input' перед нажатым елементом this */
        el_img = jQuery("#"+jQuery(this).attr("id")+"-image");
        el_id = jQuery(this).prev("input");
        window.send_to_editor = postevents_send2editor;

        /* вызываем "окно" Media Uploader */
        tb_show("Upload image or select from existent.", "media-upload.php?type=image&TB_iframe=true");

        return false;
    });

});
