<?php
/**
 * @file        functions.php
 * @description
 *
 * PHP Version  5.3.13
 *
 * @package 
 * @category
 * @plugin URI
 * @copyright   2014, Vadim Pshentsov. All Rights Reserved.
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @author      Vadim Pshentsov <pshentsoff@gmail.com> 
 * @link        http://pshentsoff.ru Author's homepage
 * @link        http://blog.pshentsoff.ru Author's blog
 *
 * @created     09.05.14
 */

function get_postevents_options() {

    //До опций пока дело не дошло
    return (object)array(

    );
}

// Добавляем в псевдо-кроновское расписание вордпресса, если события еще нет там
if(!wp_next_scheduled(POSTEVENTS_SCHEDULE_TASK)) {
    wp_schedule_event(time(), POSTEVENTS_SCHEDULE_RECURRENCE, POSTEVENTS_SCHEDULE_TASK);
    wp_cron(); //пнуть малыша
}
add_action(POSTEVENTS_SCHEDULE_TASK, 'postevents_schedule_task');
function postevents_schedule_task() {

    global $wpdb;

    $today = date(POSTEVENTS_DATE_FORMAT);
    $count = 0;

    if($wpdb->query(sprintf("SELECT * FROM `%s` WHERE `event_done` = 0", POSTEVENTS_TABLE_EVENTS))) {

        $today_events = $wpdb->last_result;

        if(!empty($today_events)) {

            foreach ($today_events as $event) {

                //Check event datetime
                if(strtotime($event->event_date) > time() ) {
                    continue;
                }

                $count++;

                if(!empty($event->post_title)) {
                    wp_update_post(array('ID' => $event->post_id, 'post_title' => $event->post_title));
                }
                if(!empty($event->post_text)) {
                    wp_update_post(array('ID' => $event->post_id, 'post_content' => $event->post_text));
                }

                if(!empty($event->post_url)) {
                    update_post_meta($event->post_id, 'forum_url', $event->post_url);
                }
                update_post_meta($event->post_id, 'featured', $event->post_featured);
                if($event->post_thumbnail_id) {
                    update_post_meta($event->post_id, '_thumbnail_id', $event->post_thumbnail_id);
                }
                if($event->post_image_id_) {
                    update_post_meta($event->post_id, 'add_image', $event->post_image_id);
                }

                //Set flag that event is done and save datetime
                $wpdb->query(sprintf("UPDATE `%s` SET `event_done` = 1, `event_done_at` = '%s' WHERE `ID`=%d", POSTEVENTS_TABLE_EVENTS, $today, $event->ID));
            }

        }
    }

}

function postevents_pagination($args) {

    $page_count = ceil($args['count']/$args['per_page']);

    $args['offset_prev'] = $args['offset'] - $args['per_page'];
    $args['enabled_prev'] = $args['offset_prev'] >= 0;

    $args['offset_next'] = $args['offset'] + $args['per_page'];
    $args['enabled_next'] = $args['offset_next'] < $args['count'];

    $args['show_nav_buttons'] = $args['count'] > $args['per_page'];

//    print_pre('$args', $args);
?>
<script language="JavaScript">
    var posts_per_page = <?php echo $args['per_page']; ?>;
    function changePerPage() {
        if(this.value != posts_per_page) {
            document.getElementsByName('action')[0].value = "<?php echo $args['action'] ?>";
            document.getElementsByName('offset')[0].value = 0;
            document.forms[0].submit();
            return true;
        }
    }
    function clickPrev() {
        document.getElementsByName('action')[0].value = "<?php echo $args['action'] ?>";
        document.getElementsByName('offset')[0].value = <?php echo $args['offset'] - $args['per_page']; ?>;
        document.forms[0].submit();
        return true;
    }
    function clickNext() {
        document.getElementsByName('action')[0].value = "<?php echo $args['action'] ?>";
        document.getElementsByName('offset')[0].value = <?php echo $args['offset'] + $args['per_page']; ?>;
        document.forms[0].submit();
        return true;
    }
</script>
    <input name="offset" type="hidden" value="<?php echo $args['offset']; ?>"/>
    <label for="per_page"><?php _e('Show per page', POSTEVENTS_TEXT_DOMAIN); ?></label>
    <select id="per_page" name="per_page" onchange="changePerPage();" style="float: none;">
        <option value="1" <?php echo (($args['per_page'] == 1) ? 'selected="selected"' : ''); ?>>1</option>
        <option value="5" <?php echo (($args['per_page'] == 5) ? 'selected="selected"' : ''); ?>>5</option>
        <option value="10" <?php echo (($args['per_page'] == 10) ? 'selected="selected"' : ''); ?>>10</option>
        <option value="25" <?php echo (($args['per_page'] == 25) ? 'selected="selected"' : ''); ?>>25</option>
        <option value="50" <?php echo (($args['per_page'] == 50) ? 'selected="selected"' : ''); ?>>50</option>
        <option value="100" <?php echo (($args['per_page'] == 100) ? 'selected="selected"' : ''); ?>>100</option>
    </select>

    <?php if($args['show_nav_buttons']) { ?>
        <input type="submit" name="prev" value="<?php _e('Previous', POSTEVENTS_TEXT_DOMAIN);?>" <?php echo ($args['enabled_prev'] ? '' : 'disabled="disabled"'); ?> onclick="clickPrev();" class="button-secondary action"/>
        <input type="submit" name="next" value="<?php _e('Next', POSTEVENTS_TEXT_DOMAIN);?>" <?php echo ($args['enabled_next'] ? '' : 'disabled="disabled"'); ?> onclick="clickNext();" class="button-secondary action"/>
    <?php } ?>
<?php
}

function print_server_time() {
    printf(__('Server time (UTC/GMT) is %s', POSTEVENTS_TEXT_DOMAIN), date(POSTEVENTS_DATE_FORMAT, time()));
}