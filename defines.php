<?php
/**
 * @file        defines.php
 * @description
 *
 * PHP Version  5.3.13
 *
 * @package 
 * @category
 * @plugin URI
 * @copyright   2014, Vadim Pshentsov. All Rights Reserved.
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @author      Vadim Pshentsov <pshentsoff@gmail.com> 
 * @link        http://pshentsoff.ru Author's homepage
 * @link        http://blog.pshentsoff.ru Author's blog
 *
 * @created     09.05.14
 */

global $wpdb;

// DB definitions
define('POSTEVENTS_PREFIX', $wpdb->prefix.'postevents_');
define('POSTEVENTS_TABLE_EVENTS', POSTEVENTS_PREFIX.'events');

define('POSTEVENTS_TEXT_DOMAIN', 'postevents');

define('POSTEVENTS_MENU_POSITION', 6);
define('POSTEVENTS_MENU_ICON', '');

define('POSTEVENTS_DATE_FORMAT', 'Y-m-d H:i:s');
define('POSTEVENTS_DATE_FORMAT_OUTPUT', 'd-m-Y H:i:s');
define('POSTEVENTS_SCHEDULE_TASK', 'postevents_task_hook');
define('POSTEVENTS_SCHEDULE_RECURRENCE', 'hourly'); //'hourly', 'twicedaily', 'daily'