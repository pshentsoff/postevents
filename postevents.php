<?php
/*
Plugin Name: Post Events
Plugin URI:
Description: Плагин для wordpress, позволяющий по расписанию менять содержимое поста.
Version: 1.0.4
Author: Vadim Pshentsov
Author URI: http://pshentsoff.ru
License: Apache License, Version 2.0
Wordpress version supported: 3.6 and above
*/
/**
 * @file        postevents.php
 * @description
 *
 * PHP Version  5.3.13
 *
 * @package 
 * @category
 * @plugin URI
 * @copyright   2014, Vadim Pshentsov. All Rights Reserved.
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @author      Vadim Pshentsov <pshentsoff@gmail.com> 
 * @link        http://pshentsoff.ru Author's homepage
 * @link        http://blog.pshentsoff.ru Author's blog
 *
 * @created     09.05.14
 */

global $wpdb;

require_once('defines.php');
require_once('functions.php');

/**
 * Инициализация плагина
 */
function postevents_init() {
    load_plugin_textdomain(POSTEVENTS_TEXT_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ).'/languages');
}
add_action('plugins_loaded', 'postevents_init');

if(is_admin()) {

    function postevents_enqueue_admin() {
        wp_enqueue_style('postevents-style', plugins_url('postevents.css', __FILE__));

        wp_enqueue_script( 'jquery' );
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');

        wp_enqueue_script('postevents-script', plugins_url('postevents.js', __FILE__));

        wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('thickbox');
    }
    add_action('admin_head', 'postevents_enqueue_admin');

    require_once(dirname( __FILE__ ) .'/admin.php');
}

/**
 * Installation tasks
 */
function postevents_install() {

    global $wpdb;

    $wpdb->query("CREATE TABLE IF NOT EXISTS `".POSTEVENTS_TABLE_EVENTS."` (
        `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `event_title` text NOT NULL,
        `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
        `event_done` tinyint(1) NOT NULL DEFAULT '0',
        `event_done_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
        `post_id` bigint(20) unsigned NOT NULL,
        `post_title` text NOT NULL,
        `post_url` varchar(255) NOT NULL,
        `post_text` longtext NOT NULL,
        `post_thumbnail_id` bigint(20) unsigned NOT NULL DEFAULT '0',
        `post_image_id` bigint(20) unsigned NOT NULL DEFAULT '0',
        `post_featured` int unsigned NOT NULL DEFAULT '0',
        PRIMARY KEY (`ID`),
        KEY `event_date` (`event_date`, `post_id`),
        KEY `post_id` (`post_id`, `event_date`)
        )
        ENGINE=MyISAM
        DEFAULT CHARSET=utf8;");

}
register_activation_hook(__FILE__,'postevents_install');

/**
 * Uninstallation tasks
 */
function postevents_uninstall() {

    global $wpdb;

    $wpdb->query('DROP TABLE IF EXISTS `'.POSTEVENTS_TABLE_EVENTS.'`');

    wp_clear_scheduled_hook(POSTEVENTS_SCHEDULE_TASK);

}
register_uninstall_hook(__FILE__, 'postevents_uninstall');

